
<?php

include_once("config.php");
include_once("autenticacao.php");

$id = $_SESSION["id"];

$result = mysqli_query($mysqli, "SELECT * FROM email_remetente WHERE destino_id = '$id'"); // using mysqli_query instead
?>

<html>
<head>	
	<title>Homepage</title>
	<link href="css/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<meta charset="UTF-8"/>
</head>

<body>
	<nav class="cyan">
    <div class="nav-wrapper">
      <a class="brand-logo" style="left: 10px;">Meus Emails</a>
      <ul id="nav-mobile" class="right hide-on-med-and-down">
        <li><a href="index.html">Sair</a></li>
      </ul>
    </div>
  </nav>
	<br>
	<div class="row">
		<div class="fixed-action-btn" class="col s12">
			<a href="add.php"  
			class="btn-floating btn-large waves-effect waves-light red accent-4 tooltipped"
			data-position="center" data-tooltip="Salvar :)" ><i class="material-icons">add</i>
		</a>
	</div>

	<div class="col s12 z-depth-4 	">

		<div class="row"></div>

		<table class=" bordered centered highlight" >

			<tr class="cyan">
				<td><b>ID_MAIL</b></td>
				<td><b>DATA</b></td>
				<td><b>E-MAIL +</b></td>
				<td hidden><b>CORPO</b></td>
				<td><b>REMETENTE</b></td>
				<td><i class="material-icons">forward</i></td>
				<td><i class="material-icons">delete</i></td>
			</tr>

			<?php 
			while($res = mysqli_fetch_array($result)) { 		
				echo "<tr>";
				echo "<td>".$res['id_email']."</td>";
				echo "<td>".$res['data_hora']."</td>";
				echo "<td><a class=\"modal-trigger\" href=\"#modal1\">".$res['titulo']."</a></td>";
				echo "<td hidden>".$res['corpo']."</td>";
				echo "<td>".$res['user_origem']."</td>";	
				echo "<td><a href=\"reencaminhar.php?id_email=$res[id_email]\"><i class=\"material-icons\">forward</i></a>";
				echo "<td><a href=\"delete.php?id_email=$res[id_email]\" onClick=\"return confirm('Você realmente deseja apagar?')\"><i class=\"material-icons\">delete</i></a></td>";	
				echo "</tr>";	
			}
			?>
		</table>
	</div>	
</div>


<div id="modal1" class="modal">
	<div class="modal-content">
		<h4  id="tituloModal"></h4>
		<p id="corpoModal"></p>
	</div>
	<div class="modal-footer">
		<a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">Fechar</a>
	</div>
</div>



</body>
<script type="text/javascript" src="js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="js/materialize.min.js"></script>

<script type="text/javascript">
$(document).ready(function(){

	$('.modal-trigger').leanModal();

	$('tr').click(function () { 
		var id = $(this).children();
		var body = id[3].innerHTML;
		var titulo = id[4].innerHTML;
		console.log(body, titulo),

		$("#corpoModal").empty();
		$("#tituloModal").empty();
		$("#corpoModal").append(body);
		$("#tituloModal").append(titulo);		
	});
});

</script>
</html>
