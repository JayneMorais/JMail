<?php
// including the database connection file
include_once("config.php");
include_once("autenticacao.php");
if(isset($_POST['reencaminhar'])){

	$titulo = mysqli_real_escape_string($mysqli, $_POST['titulo']);
	$corpo = mysqli_real_escape_string($mysqli, $_POST['corpo']);
	$destino = mysqli_real_escape_string($mysqli, $_POST['destino']);	

	if(empty($titulo) || empty($corpo) || empty($destino)) {

		if(empty($titulo)) {
			echo "<font color='red'>Título field is empty.</font><br/>";
		}
		
		if(empty($corpo)) {
			echo "<font color='red'>Corpo field is empty.</font><br/>";
		}
		
		if(empty($destino)) {
			echo "<font color='red'>Email field is empty.</font><br/>";
		}

		echo "<br/><a href='javascript:self.history.back();'>Go Back</a>";
	} else { 
		
		$origem_id = $_SESSION["id"];		
		$destino = mysqli_real_escape_string($mysqli, $_POST['destino']);
		$resultado = mysqli_query($mysqli, "SELECT * FROM usuario WHERE user = '$destino'") or die("Erro no banco de dados Aut!");
		$tot = mysqli_num_rows($resultado);

		if ($tot) {
			$dados =  mysqli_fetch_array($resultado);
			if (!strcmp($destino, $dados["user"])) {

				$destino_id = $dados["id_usuario"]; 
				$data_hora=date("Y/m/d H:i:s"); 
				
				//echo "<script>alert('Destino: $destino_id, Origem: $origem_id, Título: $titulo, Corpo: $corpo, Hora: $data_hora');</script>";

				$resultado = mysqli_query($mysqli, "INSERT INTO email(data_hora, titulo, corpo, origem_id, destino_id) 
					VALUES('$data_hora', '$titulo','$corpo', '$origem_id', '$destino_id')");
				
				echo "<font color='green'>Data added successfully.";
				header("Location: home.php");

			}else{
				echo "<script>confirm('Destino não encontrado!');</script>";
				//echo "<br/><a href='javascript:self.history.back();'>Go Back</a>";
				//header("Location: home.php");
			}		
		}
	}
}
?>


<?php

$id_email = $_GET['id_email'];

$resulta = mysqli_query($mysqli, "SELECT * FROM email WHERE id_email='$id_email'");
$tota = mysqli_num_rows($resulta);

if ($tota) {
	$res =  mysqli_fetch_array($resulta);
	if (!strcmp($id_email, $res["id_email"])) {
		$titulo = $res['titulo'];
		$corpo = $res['corpo'];
		$data_hora = $res['data_hora'];
		$data_hora=date("Y/m/d H:i:s");
	}
	else{
		echo "<script>confirm('Destino não encontrado!');</script>";
		echo "<br/><a href='javascript:self.history.back();'>Go Back</a>";
		header("Location: home.php");
	}
}

?>
<html>
<head>	
	<title>Edit Data</title>
	<link href="css/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<meta charset="UTF-8"/>
</head>

<body>
	<nav class="cyan">
    <div class="nav-wrapper">
      <a class="brand-logo" style="left: 10px;">Reecaminhar</a>
      <ul id="nav-mobile" class="right hide-on-med-and-down">
        <li><a href="index.html">Sair</a></li>
        <li><a href="home.php">Home</a></li>
      </ul>
    </div>
  </nav>
	
	<div class="row"> 
		<div class="col s10 z-depth-4 offset-s1">
			<div class="row"></div>
			<form name="form1" method="post" action="reencaminhar.php">
				<table class="striped">	
					<tr> 
						<td>Titulo</td>
						<td><input type="text" name="titulo" value="<?php echo $titulo;?>"></td>
					</tr>
					<tr> 
						<td>Corpo</td>
						<td><input type="text" name="corpo" value="<?php echo $corpo;?>"></td>
					</tr>
					<tr> 
						<td>Destino</td>
						<td>

							<select  name="destino" id="destino">
								<option value="" disabled selected>Escolha o destinatário</option>

								<?php
								$resu = mysqli_query($mysqli, "SELECT user FROM usuario ORDER BY user") or die("Erro no banco de dados Aut!");
								$to = mysqli_num_rows($resu);

								if ($to) {
									while ($mydados = mysqli_fetch_array($resu) ) {
										$vuser = $mydados["user"];			
										echo "<option value=\"$vuser\"> $vuser </option>";
									}
								}	
								?>
							</select> 

						</td>
					</tr>
					<tr>
						<td>
							<button class="btn waves-effect waves-light" type="submit" name="reencaminhar">Reencaminhar
								<i class="material-icons right">send</i>
							</button> 
						</td>
					</tr>
				</table>
			</form>
		</div>
	</div>

	<script type="text/javascript" src="js/jquery-1.11.2.min.js"></script>
	<script type="text/javascript" src="js/materialize.min.js"></script>
	<script> 
	$(document).ready(function() {
		$('select').material_select();
	}); 
	</script>
</script>


</body>
</html>
